#!/usr/bin/env python
import IsingFunctions as isif #custom module containing the metropolis algorithm as well as other necessary functions to simulate the Ising model
import IsingPlot as ispl #custom plotting module
import numpy as np

d = 100 #data points
N = 12 #matrix length
steps = 2000 #equilibrium steps and monte carlo steps

Energy = np.zeros(d) #initially set the energy as an array of length d filled with zeroes
Magnetization = np.zeros(d) #initially set the magnetization as an array of length d filled with zeroes
SpecHeat = np.zeros(d) #initially set the specific heat as an array of length d filled with zeroes 
Susceptibility = np.zeros(d) #initially set the susceptibility as an array of length d filled with zeroes

T = np.linspace(0.1, 4, d) #temperature as an array of d points between 0 and 4
J = 1 #exchange energy
h = 0 #external magnetic field

for m in range(len(T)):
    E = M = Esqr = Msqr = 0 
    config = isif.Initialize(N)
    for i in range(steps): #first for loop is to ensure that the system reaches equilibrium
        isif.Metropolis(config, T[m], N, J, h) #performs the Metropolis algorithm on the matrix at a temperature T[m]
    for i in range(steps):
        isif.Metropolis(config, T[m], N, J, h)
        En = isif.Energy(config, N, J, h) #gives the energy for each iteration
        Mag = isif.Magnetization(config) #gives the magnetization for each iteration
        E += En #increments the energy for each iteration
        M += Mag #increments the magnetization for each iteration
        Esqr += En*En #square of the energy
        Msqr += Mag*Mag #square of the magnetization
        Energy[m] = E/(steps*N*N) 
        Magnetization[m] = M/(steps*N*N)
        SpecHeat[m] = ((Esqr/steps)-((E*E)/(steps*steps)))/(N*T[m]*T[m]) #mean value of the square of the energy minus the square of the mean value of the energy
        Susceptibility[m] = ((Msqr/steps)-((M*M)/(steps*steps)))/(N*T[m]) #mean value of the square of the magnetization minus the square of the mean value of the magnetization

ispl.PlotFigs(T, Energy, Magnetization, SpecHeat, Susceptibility) #plots the energy, magnetization, specific heat, and susceptibility as functions of temperature