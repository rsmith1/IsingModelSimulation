#!/usr/bin/env python

import numpy as np
from numpy.random import rand


#The Initialize function sets the initial state of the lattice as an NxN matrix of +1's and -1's

def Initialize(N):
    initial = np.random.choice((-1,1), size=(N,N)) #creates an NxN matrix of +1's and -1's      
    return initial

#The Metropolis function scans the matrix in order to minimise the lattice energy by Monte Carlo methods

def Metropolis(config, T, N, J, h): #takes arguments configuration, temperature, matrix length, exchange energy, external magnetic field
    for i in range(N):
        for j in range(N):
            x = np.random.randint(0, N) #picks random number between 0 and N
            y = np.random.randint(0, N) #picks random number between 0 and N
            s = config[x, y] #lattice site [x,y] contribution
            nn = config[(x+1)%N, y] + config[x, (y+1)%N] + config[(x-1)%N, y] + config[x, (y-1)%N] #nearest neighbour contributions
            hi = (J * nn) - h #inner field
            delH = -2 * s * hi #energy change
            B = 1 /(T) #Boltzmann constant excluded but addressed in the units of temperature
            p_flip = np.exp(delH * B) #probability that the spin will flip
            if delH > 0: #if the energy of the system increases
                s *= -1 #flip the spin
            elif rand() < p_flip: #otherwise flips spin based on Maxwell-Boltzmann probability
                s *= -1
            config[x, y] = s
    return config #returns the configuration of the system

#The Energy function calculates the energy change of the system as it goes towards equilibrium

def Energy(config, N, J, h): 
	H = 0 # initially set the energy to zero
	for i in range(len(config)):
		for j in range(len(config)):
			s = config[i, j]
			nn = config[(i+1)%N, j] + config[i, (j+1)%N] + config[(i-1)%N, j] + config[i, (j+1)%N] #nearest neighbour contributions
			hi = (J * nn) - h #the inner field of spin i			
			delH = -2 * s * hi
			H += delH 
	return 0.5 * H #halved to avoid double counting

#The Magnetization function calculates the net magnetization of the system as it approaches equilibrium

def Magnetization(config):
	mag = np.sum(config) # the net magnetization is simply the sum of the spins
	return mag