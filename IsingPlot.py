#!/usr/bin/env python

import matplotlib.pyplot as plt

def PlotFigs(T, Energy, Magnetization, SpecificHeat, Susceptibility):

	plt.figure() #creates a new figure
	plt.plot(T, Energy,'+', color="red", label='Energy') #plots the energy as a function of temperature
	plt.xlabel("Temperature (J/k$_{B}$)") #temperature is in units of exchange energy per Boltzmann constant
	plt.ylabel("Energy")
	plt.legend(loc='best') #places legend in best possible location, i.e. not covering data points

	plt.figure()
	plt.plot(T, abs(Magnetization), '+', color="blue", label='Magnetization') #plots the magnetization as a function of temperature
	plt.xlabel("Temperature (J/k$_{B}$)")
	plt.ylabel("Magnetization")
	plt.legend(loc='best')

	plt.figure()
	plt.plot(T, SpecificHeat, '+', color="green", label='Specific Heat') #plots the the specific heat as a function of temperature
	plt.xlabel("Temperature (J/k$_{B}$)")
	plt.ylabel("Specific Heat")
	plt.legend(loc='best')

	plt.figure()
	plt.plot(T, Susceptibility, '+', color="magenta", label='Susceptibility') #plots the susceptibility as a function of temperature
	plt.xlabel("Temperature (J/k$_{B}$)")
	plt.ylabel("Susceptibility")
	plt.legend(loc='best')

	plt.show() #shows all for figures
