touch IsingFunctions.py #creates a file to place functions in and use as a custom module
touch IsingPlot.py 

mkdir /home/ross/python_bin #create a directory called python_bin to store my modules in.

mv IsingFunctions.py /home/ross/python_bin #moves the module files into the directory
mv IsingPlot.py /home/ross/python_bin

export PYTHONPATH="${PYTHONPATH}:/home/ross/python_bin" #puts the directory python_bin into the python path

cd /home/ross/python_bin #changes to the python directory

#after the modules have been written

chmod +x IsingFunctions.py #makes the modules executable
chmod +x IsingPlot.py

cp IsingFunctions.py IsingFunctions_backup.py #creates backup files for the modules
cp IsingPlot.py IsingPlot_backup.py
